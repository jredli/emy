<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/password_change', 'ProfileController@changePassword')->name('password.change');

Route::post('/password_save', 'ProfileController@saveNewPassword')->name('password.save');

Route::get('/status/{msg}', 'ProfileController@status')->name('status');
Route::get('/passChanged', 'ProfileController@status')->name('status');

Route::resource('user', 'ProfileController')->except([
    'destroy', 'create', 'store',
])->middleware('verified');

Route::post('update_profile', 'ProfileController@updateProfile')->name('user.updated')->middleware('verified');

Route::get('{handle}', ['as' => 'handle', 'uses' => 'ProfileController@show']);
