<?php

namespace App\Http\Controllers;

use App\Traits\UploadTrait;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    use UploadTrait;
    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }



    /**
     * Display the specified resource.
     *q
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param $handle
     * @return void
     */
    public function show($handle)
    {
        $user = User::where('handle', $handle)->first();

        if($user) {
            return view('users.show_user', ['user' => $user]);
        }
        else {
            return view('users.show_user');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('profile', ['user' => Auth::user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        // Get current user
        $user = User::findOrFail(auth()->user()->id);
        // Set user name
        $user->first_name = $request->input('first_name') ? $request->input('first_name') : '';
        $user->last_name = $request->input('last_name') ? $request->input('last_name') : '';
        $user->handle = $request->input('handle') ? $request->input('handle') : '';
        $user->email = $request->input('email') ? $request->input('email') : '';
        $user->crypto_wallet_address = $request->input('crypto_wallet_address') ? $request->input('crypto_wallet_address') : '';
        $user->iban = $request->input('iban') ? $request->input('iban') : '';
        $user->public_nickname = $request->input('publicName') ? $request->input('publicName') : '';
        $user->company = $request->input('company') ? $request->input('company') : '';

        // Check if a profile image has been uploaded
        if ($request->has('profile_image')) {
            // Get image file
            $image = $request->file('profile_image');
            // Make a image name based on user name and current timestamp
            $name = str_slug($request->input('name')).'_'.time();
            // Define folder path
            $folder = '/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $user->profile_image = $filePath;
        }
        // Persist user record to database
        $user->save();

        // Return user back and show a flash message
        return redirect()->back()->with(['status' => 'Profile updated successfully.']);
    }

    public function status($msg) {
        return view('status')->with('msg', $msg);
    }

    public function changePassword() {
        return view('change_pass')->with('usr', Auth::user()->id);
    }

    public function saveNewPassword(Request $request) {
        $user = User::find(Auth::user()->id);

        $newPassword = Hash::make($request->get('password'));

        $user->password = $newPassword;
        $user->save();

        $data['pass'] = true;
        $data['usr'] = Auth::user()->id;

        return view('passChange', $data);
    }
}
