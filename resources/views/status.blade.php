@extends('layouts.emy')

@section('content')
    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>


        @if(isset($msg))

            <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">
                <img width="74" class="rounded-circle" height="75" src="{{ asset('/images/checkmark.svg') }}" alt="logo" />
                <a href="#"><h2>Your password has been <br> changed</h2></a>
                <a href="{{ route('login') }}"><button class="button-link">
                        {{ __('Proceed to your account') }}
                    </button></a>
            </div>
        @endif
    </div>
@endsection
