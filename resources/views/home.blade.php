@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    @if (session('verifySuccess'))
        <div class="alert alert-success" role="alert">
            Email verification successful!
        </div>
    @endif

    @if (session('linkExpired'))
        <div class="alert alert-success" role="alert">
            Link expired :(
        </div>
    @endif

    @if (session('alreadyVerified'))
        <div class="alert alert-success" role="alert">
            alreadyVerified
        </div>
    @endif

    @auth
        You are logged in!
    @endauth
@endsection
