@extends('layouts.emy')

@section('content')
    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>


        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">

            @if(!empty($user))
                    @if(!$user['profile_image'])
                        <img class="rounded-circle text-center" style="margin: 0 auto" width="175" height="175" align="center" src="{{ asset('images/default_user.png') }}"><br/>
                    @else

                        <img class="rounded-circle text-center" style="margin: 0 auto" width="175" height="175" align="center" src="{{ asset( $user['profile_image']) }}"><br/>
                    @endif
                        <h1>Profile</h1>
                        <p class="title">@handle: {{ $user['handle'] }}</p><br/>

                        @if($user['public_nickname'] == 1)
                            <p class="title">First name: {{ $user['first_name'] }}</p><br/>
                            <p class="title">Last name: {{ $user['last_name'] }}</p><br/>
                        @elseif($user['public_nickname'] == 2)
                            <p>Public name: {{ $user['company'] }}</p><br/>
                        @else
                            <p class="title">User still didn't select this option.</p><br/>
                        @endif

                        <p class="title">Crypto wallet address: {{ $user['crypto_wallet_address'] }}</p><br/>
                        <p class="title">IBAN: {{ $user['iban'] }}</p><br/>

                        @else
                            <img width="74" class="rounded-circle" height="75" src="{{ asset('/images/error.png') }}" alt="logo" />
                            <span class="user-name">Couldn't find user</span>
                            <h2>User with such @handle doesn't exist.</h2>
                        @endif

                </div>
    </div>

@endsection
