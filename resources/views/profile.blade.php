@extends('layouts.emy')

@section('content')

    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="#"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>
        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup user-profile-edit">

            @if (auth()->user()->profile_image)
                <img width="74" class="rounded-circle" height="75" src="{{ auth()->user()->profile_image }}">
            @else
                <img width="74" class="rounded-circle" height="75" src="{{ asset('/images/profile_icon.svg') }}" alt="logo" />
            @endif

            <span class="user-name">Hello {{ $user->first_name }} {{ $user->last_name }}</span>
            <h2>We will let you know as soon as we launch the app</h2>
            <h2>Profile informations</h2>

                <form method="POST" enctype="multipart/form-data" action="{{ route('user.updated') }}">
                @csrf
                <div class="input-box">
                    <input id="first_name" type="text" placeholder="First Name" class="@error('first_name') is-invalid @enderror" name="first_name" value="{{ $user->first_name  }}" required>
                    <i class="icon-profile"></i>
                    <label class="floating-label">First Name</label>
                    @error('first_name')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="last_name" type="text" placeholder="Last Name" class=" @error('last_name') is-invalid @enderror" name="last_name" value="{{ $user->last_name }}" required>
                    <i class="icon-profile"></i>
                    <label class="floating-label">Last Name</label>
                    @error('last_name')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="email" type="email" placeholder="Email" class=" @error('email')is-invalid @enderror" name="email" value="{{ $user->email }}" required>
                    <i class="icon-email"></i>
                    <label class="floating-label">Email</label>
                    @error('email')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="company" type="text" placeholder="Company name" class="@error('company') is-invalid @enderror" name="company" value="{{ $user->company  }}" required>
                    <i class="icon-profile"></i>
                    <label class="floating-label">Company name</label>
                    @error('company')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="handle" type="text" placeholder="Handle" name="handle" value="{{ $user->handle  }}" required>
                    <i class="icon-handle"></i>
                    <label class="floating-label">Handle</label>
                    @error('handle')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>
                <div class="input-box" style="display: flex">
                    <label>Crypto wallet address</label>
                    <div class="radio">
                        <input id="radio-1" name="publicName" value="1" type="radio" {{ $user->public_nickname == 1 ? 'checked' : '' }}>
                        <label for="radio-1" class="radio-label">Personal Name</label>
                    </div>

                    <div class="radio">
                        <input id="radio-2" name="publicName" value="2" type="radio" {{ ($user->public_nickname == 2) ? 'checked' : '' }}>
                        <label  for="radio-2" class="radio-label">Company Name</label>
                    </div>
                    @error('publicName')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                    @enderror
                </div>
                <div class="input-box">
                    <div class="input-group mb-3">
                        <div class="custom-file">
                            <input id="profile_image" class="custom-file-input" type="file" name="profile_image">
                            <label class="custom-file-label" for="inputGroupFile01">Choose profile image</label>
                        </div>
                        @error('profile_image')
                        <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                        @enderror
                    </div>
                </div>
                <div class="input-box">
                    <input id="crypto_wallet_address" type="text" placeholder="Crypto wallet address" class=" @error('crypto_wallet_address') is-invalid @enderror" name="crypto_wallet_address" value="{{ $user->crypto_wallet_address }}" required>
                    <i class="icon-handle"></i>
                    <label class="floating-label">Crypto wallet address</label>
                    @error('crypto_wallet_address')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>

                <div class="input-box">
                    <input id="iban" type="text" placeholder="IBAN" class=" @error('iban') is-invalid @enderror" name="iban" value="{{ $user->iban }}" required>
                    <i class="icon-handle"></i>
                    <label class="floating-label">IBAN</label>
                    @error('iban')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>


                <div class="input-box">
                    <input id="password" type="password" placeholder="Password" class=" @error('password') is-invalid @enderror" name="password" value="{{ $user->password }}" disabled>
                    <i class="icon-password"></i>
                    <label class="floating-label">Password</label>
                    @error('password')
                    <span class="validation-alert" role="alert">
                        {{ $message }}
                    </span>
                    @enderror
                    <a href="{{ route('password.change') }}"><u>Change password</u></a>
                </div>
                <p class="signup-terms">
                    By signing up you agree to our <br />
                    <a href="#">Terms & Conditions</a> and
                    <a href="#">Privacy policy</a>
                </p>
                <button type="submit">{{ __('Save') }}</button>
            </form>
        </div>
    </div>

@endsection
