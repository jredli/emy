@extends('layouts.emy')

@section('content')

    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>
        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">

            @if (session('status'))

                <h2>Check your email</h2>
                <span class="forgot-pass">We have sent a reset password email. <br/>
                Please click the reset password<br/>
                link to set your new password</span>

                <span class="forgot-pass">Didn't receive the email?</span>

                <a href="{{ route('verification.resend') }}" class="button-link">Resend email</a>
            @else
                <h2>Forgot your password?</h2>
                <span class="forgot-pass">Don't worry, resetting password is easy, just tell us the <br/> email address you registered</span>
                <form method="POST" action="{{ route('password.email')  }}">
                    @csrf

                    <div class="input-box">
                        <input id="email" type="email" placeholder="Email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                        <i class="icon-email"></i>
                        <label class="floating-label">Email*</label>
                        @error('email')
                        <span class="validation-alert" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <button class="pass-send-btn" type="submit">{{ __('Send') }}</button>

                    <p class="signup-terms">
                        <span>Do no want to change the password?</span>
                        <a href="{{ route('home') }}"><u>Go back</u></a>
                    </p>
                </form>
            @endif
        </div>
    </div>

@endsection
