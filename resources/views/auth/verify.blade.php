@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h2 style="margin-top: 0">{{ __('Verify Your Email Address') }}</h2>
        <h3>
            @if (session('resent'))
                <div class="alert alert-success" role="alert">
                    {{ __('New link was just sent!') }}
                </div>
            @endif

                {{ __('Before proceeding, please check your email for a verification link.') }}
                {{ __('If you did not receive the email') }}, <u><a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a></u>.
        </h3>
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Verify Your Email Address') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    @if (session('resent'))--}}
{{--                        <div class="alert alert-success" role="alert">--}}
{{--                            {{ __('New link was just sent!') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    {{ __('Before proceeding, please check your email for a verification link.') }}--}}
{{--                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</div>
@endsection
