@extends('layouts.emy')

@section('content')

    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>
        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">
            <h2>Welcome back</h2>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-box">
                    <input id="email" type="text" placeholder="Email"  @error('email') is-invalid @enderror" name="email" value="{{ old('email ') }}" required autofocus>
                    <i class="icon-profile"></i>
                    <label class="floating-label">Email</label>
                    @error('email')
                    <span class="validation-alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="password" type="password" placeholder="Password" @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required>
                    <i class="icon-password"></i>
                    <label class="floating-label">Password</label>
                    @error('password')
                    <span class="validation-alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>


                <p class="signup-terms">
                    <a href="{{ route('password.request') }}"><u>Forgot password?</u></a>
                </p>
                <button type="submit">{{ __('Login') }}</button>
                <p class="signup-terms">
                    <span>Don't have an account?</span>><br/>
                    <a href="{{ route('register') }}"><u>Reserve your Emyapp handle</u></a>
                </p>
            </form>
        </div>
    </div>

@endsection
