@extends('layouts.emy')

@section('content')

    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>
        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">
            <h2>Reserve your handle here</h2>
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="input-box">
                    <input id="first_name" type="text" placeholder="First Name" class=" @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required>
                    <i class="icon-profile"></i>
                    <label class="floating-label">First Name</label>
                    @error('first_name')
                        <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="input-box">
                    <input id="last_name" type="text" placeholder="Last Name" class=" @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required>
                    <i class="icon-profile"></i>
                    <label class="floating-label">Last Name</label>
                    @error('last_name')
                    <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="email" type="email" placeholder="Email" class=" @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                    <i class="icon-email"></i>
                    <label class="floating-label">Email*</label>
                    @error('email')
                    <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
                <div class="input-box">
                    <input id="handle" type="text" placeholder="Choose handle" class=" @error('handle') is-invalid @enderror" name="handle"
                           value="@if($handleReserve){{ ($handleReserve) }}@else {{ old('handle') }} @endif"
                           required>
                    <i class="icon-handle"></i>
                    <label class="floating-label">Choose handle</label>
                    @error('handle')
                    <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="input-box">
                    <input id="password" type="password" placeholder="Choose password" class=" @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required>
                    <i class="icon-password"></i>
                    <label class="floating-label">Choose password</label>
                    @error('password')
                    <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>

                <div class="input-box">
                    <input id="password_confirmation" type="password" placeholder="Confirm password" class=" @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
                    <i class="icon-password"></i>
                    <label class="floating-label">Confirm password</label>
                    @error('password_confirmation')
                    <span class="validation-alert" role="alert">
                            {{ $message }}
                        </span>
                    @enderror
                </div>
                <p class="signup-terms">
                    By signing up you agree to our <br />
                    <a href="#">Terms & Conditions</a> and
                    <a href="#">Privacy policy</a>
                </p>
                <button type="submit">{{ __('Reserve') }}</button>
            </form>
        </div>
        </div>
    </div>

@endsection
