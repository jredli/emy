@extends('layouts.emy')

@section('content')
    <div class="row">
        <div class="d-none d-md-flex col-md-6 left-signup">
            <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
            <h1>Welcome to <br />Emyapp!</h1>
            <h2>A Twitter for payments.</h2>
        </div>


        <div class="col-12 col-md-6  offset-xl-1 col-xl-5 right-signup">
            <h2>Please enter your new <br> password</h2>
            <form method="POST" action="{{ route('password.save') }}">
                @csrf
                <div class="input-box">
                    <input id="password" type="password" placeholder="New Password" @error('password') class="is-invalid @enderror" name="password" value="{{ old('password') }}" required>
                    <i class="icon-password"></i>
                    <label class="floating-label">New Password</label>
                    @error('password')
                    <span class="validation-alert">
                        {{ $message }}
                    </span>
                    @enderror
                </div>

                <div class="input-box">
                    <input id="password-confirm" type="password" placeholder="Repeat Password" class="@error('password') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" required>
                    <i class="icon-password"></i>
                    <label class="floating-label">Repeat Password</label>
                </div>

                <p class="signup-terms">
                    <button type="submit" class="button-link">
                        {{ __('Save password') }}
                    </button>
                    <span>Do not want to change password?</span><br/>
                    <a href="{{ URL::previous()  }}"><u>Go back</u></a>
                </p>
            </form>

    </div>
    </div>
@endsection
