<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Emy - Crypto for humans</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" type="image/png" href="https://www.getemy.com/media/favicon/favicon-32x32.png" sizes="32x32">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<!-- input-section -->
<section class="background-color-dark input-section">
    <div class="container screens">
        <div class="row">
            <div class="col-12 index-nav">
                <a href="{{ route('home') }}"><img src="{{ asset('/media/logo/logo.svg') }}" alt="logo" /></a>
                @guest
                        <a href="{{ route('register') }}" class="reserve-index-btn">{{ __('Reserve') }}</a>
                        <a href="{{ route('login') }}" id="login-btn" class="reserve-index-btn">{{ __('Login') }}</a>
                @else



                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('user.edit', Auth::user()->id ) }}">
                                {{ __('Profile') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>

                @endguest
            </div>
        </div>
        <div class="row">
            <div
                class="col-12 offset-md-1 col-md-11 offset-xl-0 col-xl-12 screen-1">
                <h2>@yield('content')</h2>
                <h2>Emy - Crypto for humans!</h2>
                <h3>
                    With your public handle you can receive Crypto in seconds.
                </h3>
                <div class="yourhandle">
                    <form action="{{ route('register') }}" method="GET">
                    @csrf
                        <label for="handle">@</label>
                        <input type="text" name="handleReserve" placeholder="yourhandle" />


                        <button type="submit" name="reserve-btn" id="reserve-btn">
                            Reserve<i class="icon-arrow"></i>
                        </button>

                    </form>
                </div>
                <p>
                    <span>Emyapp is twitter for payments.</span> All you need is an
                    emyapp handle.
                </p>
            </div>
        </div>
    </div>
</section>
<!-- slides-section -->
<section class="background-color-dark slides-section">
    <!-- slide 1 -->
    <div class="container screens">
        <div class="row screen-2 d-flex">
            <div class="col-12 col-md-7 col-lg-8">
                <div class="text-box box-active d-flex">
                    <span class="number-large">01</span>
                    <div>
                        <h2>
                            <span class="number-small">01</span>Create your Emyapp handle
                        </h2>
                        <p>
                            Every Emyapp handle is unique and is at the same time your own
                            Profile for your payments - your unique Emmyapp Profile.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <picture>
                    <source
                        srcset="{{ asset('/media/mobile/images/slide-1-mob.png') }}"
                        media="(max-width: 767px)"
                    />
                    <source
                        srcset="{{ asset('/media/tablet/images/slide-1-tab.png') }}"
                        media="(max-width: 922px)"
                    />
                    <img
                        class="img-fluid animate-me-img"
                        src="{{ asset('/media/desktop/images/slide-1-desk.png') }}"
                        alt="emy phone"
                    />
                </picture>
            </div>
        </div>
    </div>

    <!-- slide 2 -->
    <div class="container screens">
        <div class="row screen-2 d-flex">
            <div class="col-12 col-md-7 order-md-2 col-lg-8">
                <div class="text-box box-active d-flex justify-content-end">
                    <span class="number-large">02</span>
                    <div>
                        <h2>
                            <span class="number-small">02</span>Share your Emyapp profile
                        </h2>
                        <p>
                            Share your Emy Handle. Everyone visiting your profile can send you Bitcoin, Ethereum and Ripple (and much Cryptos to come), straight in any Browser, hassle-free.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <picture>
                    <source
                        srcset="{{ asset('/media/mobile/images/slide-2-mob.png') }}"
                        media="(max-width: 767px)"
                    />
                    <source
                        srcset="{{ asset('/media/tablet/images/slide-2-tab.png') }}"
                        media="(max-width: 922px)"
                    />
                    <img
                        class="img-fluid animate-me-img left-img"
                        src="{{ asset('/media/desktop/images/slide-2-desk.png') }}"
                        alt="emy phone"
                    />
                </picture>
            </div>
        </div>
    </div>
    <!-- slide 3 -->
    <div class="container screens">
        <div class="row screen-2 d-flex">
            <div class="col-12 col-md-7 col-lg-8">
                <div class="text-box box-active d-flex">
                    <span class="number-large">03</span>
                    <div>
                        <h2><span class="number-small">03</span>Get payed instantly</h2>
                        <p>
                            As soon as you have your Emyapp handle you can accept payments
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4">
                <picture>
                    <source
                        srcset="{{ asset('/media/mobile/images/slide-3-mob.png') }}"
                        media="(max-width: 767px)"
                    />
                    <source
                        srcset="{{ asset('/media/tablet/images/slide-3-tab.png') }}"
                        media="(max-width: 922px)"
                    />
                    <img
                        class="img-fluid animate-me-img"
                        src="{{ asset('/media/desktop/images/slide-3-desk.png') }}"
                        alt="emy phone"
                    />
                </picture>
            </div>
        </div>
    </div>
</section>
<!-- twitter-payments -->
<section class="container twitter-payments">
    <div class="row">
        <div class="col-12 col-sm-3 offset-sm-5  col-lg-6 offset-lg-6">
            <h2>Twitter for your coins - couldn't be more simple</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-4 col-sm-5  col-lg-6">
            <picture>
                <source
                    srcset="{{ asset('/media/mobile/images/mobile.png') }}"
                    media="(max-width: 576px)"
                />
                <source
                    srcset="{{ asset('/media/tablet/images/mobile.png') }}"
                    media="(max-width: 922px)"
                />
                <img
                    src="{{ asset('/media/desktop/images/mobile.png') }}"
                    alt="mobile phone"
                />
            </picture>
        </div>
        <div class="col-8 col-sm-5  col-lg-5">
            <p>
                With your Emyapp handle everyone can transfer money to you at any
                time. <br />
                Even better, the sender can decide how he wants to transfer it.
                There are no limits.
            </p>
        </div>
    </div>
</section>
<!-- icons-section -->
<section class="container icons-section">
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img
                src="{{ asset('/media/mobile/icons/icons8-online_payment.svg') }} "
                alt=""
            />
            <h3>Easy setup - Instant Payment</h3>
            <p>
                Get paid instantly on your Emyapp. You only need to reserve your
                handle and you are able to accept payments right away.
            </p>
        </div>

        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img src="{{ asset('/media/mobile/icons/icons8-checked.svg') }}" alt="" />
            <h3>Instant confirmations</h3>
            <p>
                Be notified instantly about a payment to your account. Confirm the
                payer the very moment he sent the payment.
            </p>
        </div>

        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img
                src="{{ asset('/media/mobile/icons/icons8-receive_change.svg') }}"
                alt=""
            />
            <h3>Pay and swap Bitcoin, Ethereum and Ripple</h3>
            <p>
                If someone wants to pay you, he can choose to pay you in Bitcoin, Ethereum or Ripple (more Coins to come).
            </p>
        </div>

        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img src="{{ asset('/media/mobile/icons/icons8-no_apple.svg') }}" alt="" />
            <h3>No app / No POS</h3>
            <p>
                It couldn’t be more simple. Get your Emyapp handle and that’s it.
                You can start accepting payments.
            </p>
        </div>

        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img src="{{ asset('/media/mobile/icons/icons8-private2.svg') }}" alt="" />
            <h3>Secure</h3>
            <p>
                Due to a network of a high-quality partners we can ensure the safety
                of your assets. Withdrawal of funds can only happen with proper KYC
                and 2FA. Highest security standards for your money.
            </p>
        </div>

        <div class="col-12 col-md-6 col-lg-4 icons-box">
            <img
                src="{{ asset('/media/mobile/icons/icons8-geography_filled.svg') }}"
                alt=""
            />
            <h3>Borderless</h3>
            <p>
                Get paid locally and globally with your Emyapp handle.
            </p>
        </div>
    </div>
</section>
<!-- online-payments -->
<section class="container online-payments">
    <div class="row">
        <div class="col-12 col-sm-6">
            <h2>Emy accept Payments Online or even through Social Media</h2>
            <p>
                Just point your friends to your Emyapp Profile and they can pay you
                instantly.<br />
                We offer simple ways to seamlessly integrate a payment experience on
                your website or social media pages.
            </p>
        </div>
        <div class="col-12 col-sm-6">
            <h3>Facebook pages</h3>
            <picture>
                <source
                    srcset="{{ asset('/media/mobile/images/facebook-pages.png') }}"
                    media="(max-width: 576px)"
                />
                <source
                    srcset="{{ asset('/media/tablet/images/facebook-pages.png') }}"
                    media="(max-width: 922px)"
                />
                <img
                    class="img-fluid"
                    src="{{ asset('/media/desktop/images/facebook-pages.png') }}"
                    alt="emmyapp facebook"
                />
            </picture>
            <h3>Wordpress plugin</h3>
            <img
                class="emyapp-checkout"
                src="{{ asset('/media/mobile/icons/EMYAPP.png') }}"
                alt="emyapp checkout available"
            />
            <h3>Shopify</h3>
            <img
                class="emyapp-checkout"
                src="{{ asset('/media/mobile/icons/EMYAPP.png') }}"
                alt="emyapp checkout available"
            />
        </div>
    </div>
</section>
<!-- pay-coffee -->
 <section class="container pay-coffee">
    <div class="row">
        <div class="col-12 col-md-7 order-2">
            <picture>
                <source
                    srcset="{{ asset('/media/mobile/images/get_that_coffee_paid.png') }}"
                    media="(max-width: 576px)"
                />
                <source
                    srcset="{{ asset('/media/tablet/images/get_coffee_paid.png') }}"
                    media="(max-width: 922px)"
                />
                <img
                    src="{{ asset('/media/desktop/images/get_coffee_paid.png') }}"
                    alt="mobile phone"
                />
            </picture>
        </div>
        <div class="col-12 col-md-5 d-flex order-md-2">
          <span>
            <h4>Get that coffee paid.</h4>
            <h2>
              You can accept Credit Card payments without needing a POS device.
            </h2>
            <p>
              You worry about providing excellent service, we worry about you
              getting paid.
            </p>
          </span>
        </div>
    </div>
</section>
<!-- business-solution -->
<section class="container business-solution">
    <div class="row">
        <div class="col-12">
            <h4>More power to your business</h4>
            <h2>
                Emyapp - a solution for your<span> business </span>no matter how
                small or big
            </h2>
            <div class="accordion">
                <div class="accord-1">
                    <div class="accord-img-wrap">
                        <div class="accord-text">
                            <h6>Small business</h6>
                            <p>
                                No matter how small or big your business - no matter where -
                                accept payments instantly.
                            </p>
                            <p>
                                Just finished fixing the pipe in the house of a customer of
                                yours? Sold something on ebay and want to get paid? You sell
                                local cheese at the city market and a customer has no cash
                                in his pocket? No problem, with Emyapp you can get paid and
                                receive an instant confirmation.
                            </p>
                        </div>

                        <picture>
                            <source
                                srcset="{{asset('/media/mobile/images/bussines.png')}}"
                                media="(max-width: 576px)"
                            />
                            <source
                                srcset="{{asset('/media/tablet/images/bussines.png')}}"
                                media="(max-width: 922px)"
                            />
                            <img
                                class="expand image-1 expand-focus"
                                src="{{asset('/media/desktop/images/bussines.png')}}"
                                alt="business solution"
                            />
                        </picture>
                    </div>
                    <p class="accord-text-phone">
                        Just finished fixing the pipe in the house of a customer of
                        yours? Sold something on ebay and want to get paid? You sell
                        local cheese at the city market and a customer has no cash in
                        his pocket? No problem, with Emyapp you can get paid and receive
                        an instant confirmation.
                    </p>
                    <div class="accord-close accord-close-1">
                        <h6>Friends</h6>
                        <picture>
                            <source
                                srcset="{{asset('/media/mobile/images/friends-closed.png')}}"
                                media="(max-width: 576px)"
                            />
                            <source
                                srcset="{{asset('/media/tablet/images/friends-closed.png')}}"
                                media="(max-width: 922px)"
                            />
                            <img
                                class=""
                                src="{{asset('/media/desktop/images/friends-closed.png')}}"
                            />
                        </picture>
                    </div>
                </div>

                <div class="accord-2">
                    <div class="accord-close accord-close-2">
                        <h6>Small business</h6>
                        <picture>
                            <source
                                srcset="{{asset('/media/mobile/images/bussines-closed.png')}}"
                                media="(max-width: 576px)"
                            />
                            <source
                                srcset="{{asset('/media/tablet/images/bussines-closed.png')}}"
                                media="(max-width: 922px)"
                            />
                            <img
                                class=""
                                src="{{asset('/media/desktop/images/bussines-closed.png')}}"
                            />
                        </picture>
                    </div>
                    <div class="accord-img-wrap">
                        <div class="accord-text">
                            <h6>Friends</h6>
                            <p>
                                Send and receive money between friends without having to
                                create new bank account.
                            </p>
                        </div>

                        <picture>
                            <source
                                srcset="{{asset('/media/mobile/images/friends.png')}}"
                                media="(max-width: 576px)"
                            />
                            <source
                                srcset="{{asset('/media/tablet/images/friends.png')}}"
                                media="(max-width: 922px)"
                            />
                            <img
                                class="expand image-2"
                                src="{{asset('/media/desktop/images/friends.png')}}"
                                alt="business solution"
                            />
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- footer -->
<footer class="container-fluid">
    <div class="container">
        <div class="row justify-content-center">
            <p>All rights reserved. EMYAPP 2019 copyright.</p>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script
    src="https://code.jquery.com/jquery-3.4.0.min.js"
    integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
    crossorigin="anonymous"
></script>
<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
<script src="{{ asset('js/main.js') }}" defer></script>
{{--<script src="{{ asset('js/signup.js') }}" defer></script>--}}
</body>
</html>
