function changeLink(oldHtml, newHTML) {
  var oldPath = window.location.pathname;
  var newPath;

  if (oldPath.indexOf(oldHtml)) {
    newPath = oldPath.replace(oldHtml, newHTML);
  } else {
    newPath = oldPath + newHTML;
  }

  return newPath;
}

function setAccordHeight() {
  var height = jQuery(".accord-1").height();
  jQuery(".accordion").css("min-height", height);
}

jQuery(document).ready(function() {

  // accordion logic
  setAccordHeight();
  jQuery(window).bind("resize", function() {
    setAccordHeight();
  });
  jQuery(".accord-2").slideUp(1);
  jQuery(".accord-close-2").click(function(event) {
    jQuery(".accord-2").slideUp(500, function() {
      jQuery(".accord-1").slideDown(1000);
    });
    event.preventDefault();
  });
  jQuery(".accord-close-1").click(function(event) {
    jQuery(".accord-1").slideUp(500, function() {
      jQuery(".accord-2").slideDown(1000);
    });
    event.preventDefault();
  });
});
