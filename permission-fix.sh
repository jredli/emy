#!/bin/bash

echo "Fixing group for storage folder"
chown -R $USER:www-data storage
echo "Fixing group for bootstrap/cache folder"
chown -R $USER:www-data bootstrap/cache
echo "Changing permissions for storage folder"
chmod -R 775 storage
echo "Changing permissions for bootstrap/cache folder"
chmod -R 775 bootstrap/cache