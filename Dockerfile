FROM php:7.2-apache

RUN rm -rf /var/lib/mysql/*
RUN apt-get update \
 && apt-get install -y git zlib1g-dev libicu-dev g++ nano \
 && docker-php-ext-install mbstring pdo pdo_mysql zip \
 &&  docker-php-ext-configure intl \
 && docker-php-ext-install intl \
 && a2enmod rewrite \
 && sed -i 's!/var/www/html!/var/www/public!g' /etc/apache2/sites-available/000-default.conf \
 && mv /var/www/html /var/www/public \
 && curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer

#RUN sed -i -e "s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

WORKDIR /var/www
